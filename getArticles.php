<?php
header('Access-Control-Allow-Origin:*');

require("config.php");

//get data from post with key filter
$b64Data = $_POST['filter'];

//decode base64 data to json string 
$jData = base64_decode($b64Data);

// decode back to json object to process
$oData = json_decode($jData);

//assign variable
$cur_subCategory = $oData->cur_subCategory;

//id for status data , msg describe status, data array keep the data from sql
$articles = array("id" => 0, "msg" => "", "data" => array());

$sql = "SELECT * FROM article WHERE c_id='$cur_subCategory'  ORDER BY a_document_name";

if($result = mysqli_query($conn, $sql)){
	if (mysqli_num_rows($result) > 0) {
	    // output data of each row
	    $aArticles = array();
	    while($row = mysqli_fetch_assoc($result)) {
	       $aArticles[] = $row;
	    }
	    //assign $aArticles to data in $article variable
	    $articles['data'] = $aArticles;

	    // assign value 1 to id in $article to indicate the process is success
	    $articles['id'] = 1;
	} else
	 {
		//return status 
		$articles['id'] = 1;
	}
}
else{

	//if not success, error message from sql is assign to msg in $article
	$articles['msg'] = "Error: " . $sql . "<br>" . mysqli_error($conn);
}

//encode $article to json object and assign to a variable name $jArticles
$jArticles = json_encode($articles);

//encode $jArticles to base64 for security purpose
echo base64_encode($jArticles);

//echo $jArticles;

mysqli_close($conn);
?>